<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function() {
    /*##################Login routes####################*/
    Route::get('/','Auth\AdminLoginController@login')->name('login'); 
    Route::get('/login','Auth\AdminLoginController@login'); 
    Route::post('/login','Auth\AdminLoginController@login');
    Route::get('/forgot-password','Admin\LoginController@forgotPassword'); 
    Route::post('/forgot-password','Admin\LoginController@forgotPassword'); 
    Route::get('/logout','Admin\LoginController@logout');

    /*##################Admin routes####################*/
    Route::get('/change-password','Admin\AdminController@changePassword');
    Route::post('/change-password','Admin\AdminController@changePassword');
    Route::get('/my-profile','Admin\AdminController@myProfile');
    Route::post('/my-profile','Admin\AdminController@myProfile');

     /*##################Dashboard routes####################*/
    Route::get('/dashboard','Admin\AdminController@dashboard');

     /*##################User routes####################*/
    Route::get('/users','Admin\UserController@users');
    Route::get('/user-detail/{customer_id}','Admin\UserController@userDetail');


     /*##################Category routes####################*/
    Route::get('/category','Admin\CategoryController@category'); 
    Route::get('/subcategory','Admin\CategoryController@subcategory'); 

});
Route::get('/','User\UserController@dashboard');
Route::get('register','User\UserController@register');
Route::post('register','User\UserController@register');
Route::post('city','User\UserController@City');
Route::post('skills','User\UserController@skills');

