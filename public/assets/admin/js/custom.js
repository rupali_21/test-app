$(document).ready(function() {
    $("select[name='admission_allow']").change(function() {
        var b = document.getElementById("admission_allow").value == "0";
        var c = document.getElementById("admission_allow").value == "1";
        if (b) {
            document.getElementById("date").style.display = "none";
            $('input[name="admission_start_date"]').removeClass(
                "admission_start_date"
            );
            $('input[name="admission_end_date"]').removeClass(
                "admission_end_date"
            );
        } else if (c) {
            document.getElementById("date").style.display = "block";
            $('input[name="admission_start_date"]').addClass(
                "admission_start_date"
            );

            $('input[name="admission_end_date"]').removeClass(
                "admission_end_date"
            );
        }
    });
    function hidegst() {
        var b = document.getElementById("gststatus").value == "2";
        var c = document.getElementById("gststatus").value == "1";
        var a = document.getElementById("gststatus").value == "3";
        if (b) {
            document.getElementById("gst-no").style.display = "none";
            $('input[name="gst_no"]').removeClass("gst_no");
        } else if (c) {
            document.getElementById("gst-no").style.display = "block";
            $('input[name="gst_no"]').addClass("gst_no");
        } else if (a) {
            document.getElementById("gst-no").style.display = "block";
            $('input[name="gst_no"]').addClass("gst_no");
        }
    }
    function others() {
        var b = document.getElementById("other").value == "Other";
        if (b) {
            document.getElementById("comp").style.display = "block";
            $('input[name="other_comp_type"]').addClass("other_comp_type");
        } else {
            document.getElementById("comp").style.display = "none";
            $('input[name="other_comp_type"]').removeClass("other_comp_type");
        }
    }

    $(document).ready(function() {
        $("button.reset").click(function() {
            $(".validate")
                .text("")
                .show();
        });

        $("#other").on("change", function() {
            var a = document.getElementById("other").value == "Individual";
            var b =
                document.getElementById("other").value == "Sole Propriotership";
            var c = document.getElementById("other").value == "Partnership";
            var d = document.getElementById("other").value == "Pvt Ltd";
            var e = document.getElementById("other").value == "Other";
            if (a) {
                $("#lable").text("Name of individual");
            } else if (b) {
                $("#lable").text("Name of sole proprioter");
            } else if (c) {
                $("#lable").text("Name of partnership firm");
            } else if (d) {
                $("#lable").text("Name of company");
            } else if (e) {
                $("#lable").text("Name");
            }
        });
    });
});
