jQuery(document).ready(function(){
	/* When click on change profile pic */	
	jQuery('#change-profile-pic').on('click', function(e){
        jQuery('#profile_pic_modal').modal({show:true});
        jQuery("#profile-pic").val('');

        /*if(jQuery("#profile_pic_modal img#photo").src())
        	jQuery(".imgareaselect-outer").show();*/
    });	

	//Hide the cropping box on close
    jQuery('#profile_pic_modal').on('hidden.bs.modal', function () {
	    jQuery(".imgareaselect-outer").hide();
	})

    //Step2
	jQuery('#profile-pic').on('change', function()	{ 
		jQuery("#preview-profile-pic").html('');
		jQuery("#preview-profile-pic").html('Uploading....');
		jQuery('#save_crop').attr("disabled","disabled");
		jQuery('#profile-pic').hide();

		jQuery("#cropimage").ajaxForm(
		{
			target: '#preview-profile-pic',
			success: function() { 
				jQuery('#save_crop').removeAttr("disabled");
				jQuery('#save_crop').show();
				jQuery('#profile-pic').show();
				jQuery('img#photo').imgAreaSelect({
					aspectRatio: '1:1',
					onSelectEnd: getSizes,
				});
				jQuery('#image_name').val(jQuery('#photo').attr('file-name'));
			}
		}).submit();
	});

	/* handle functionality when click crop button  */
	jQuery('#save_crop').on('click', function(e){
    	e.preventDefault();
    	params = {
            targetUrl: image_url,
            action: 'save',
            x_axis: jQuery('#hdn-x1-axis').val(),
            y_axis : jQuery('#hdn-y1-axis').val(),
            x2_axis: jQuery('#hdn-x2-axis').val(),
            y2_axis : jQuery('#hdn-y2-axis').val(),
            thumb_width : jQuery('#hdn-thumb-width').val(),
            thumb_height:jQuery('#hdn-thumb-height').val(),
            oldImg:jQuery('#oldImg').val(),
        };
        saveCropImage(params);
    });

    /* Function to get images size */
    function getSizes(img, obj){
        var x_axis = obj.x1;
        var x2_axis = obj.x2;
        var y_axis = obj.y1;
        var y2_axis = obj.y2;
        var thumb_width = obj.width;
        var thumb_height = obj.height;
        if(thumb_width > 0) {
			jQuery('#hdn-x1-axis').val(x_axis);
			jQuery('#hdn-y1-axis').val(y_axis);
			jQuery('#hdn-x2-axis').val(x2_axis);
			jQuery('#hdn-y2-axis').val(y2_axis);
			jQuery('#hdn-thumb-width').val(thumb_width);
			jQuery('#hdn-thumb-height').val(thumb_height);
        } else {
            alert("Please select portion..!");
		}
    }
    /* Function to save crop images */
    function saveCropImage(params) {
		jQuery.ajax({
			url: params['targetUrl'],
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			cache: false,
			dataType: "json",
			data: {
				action: params['action'],
				id: jQuery('#hdn-profile-id').val(),
				t: 'ajax',
				w1:params['thumb_width'],
				x1:params['x_axis'],
				h1:params['thumb_height'],
				y1:params['y_axis'],
				x2:params['x2_axis'],
				y2:params['y2_axis'],
				image_name :jQuery('#image_name').val(),
				oldImg:params['oldImg'],
			},
			type: 'Post',
		   	success: function (response) {
					jQuery('#profile_pic_modal').modal('hide');
					jQuery(".imgareaselect-border1,.imgareaselect-border2,.imgareaselect-border3,.imgareaselect-border4,.imgareaselect-border2,.imgareaselect-outer").css('display', 'none');jQuery("#l_profile_picture").show()
					jQuery("#l_profile_picture").attr('src', response['image_path']);

					if(response['login']===true){
						setTimeout(function(){ location.reload(); }, 1000);

					}else{
						jQuery("#lawyer_image").show();
						jQuery("#lawyer_image").val(response['image_name']);
					}

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert('status Code:' + xhr.status + 'Error Message :' + thrownError);
			}
		});
    }
});