@extends('admin.layouts.app')
@section('content')
<style>
table {
  
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  /* background-color: #dddddd; */
}
</style> 
    <div class=" br-profile-page">
        <div class="tab-content br-profile-body"> 
            <div class="tab-pane fade active show" id="posts">
                <div class="row"> 
                    <div class="col-lg-12">
                        <div class="card pd-20 pd-xs-30 shadow-base bd-0">
                            <?php   $city=CommonFunction::getSingleField('cities','city','id',$user->city);
                            $state=CommonFunction::getSingleField('states','name','id',$user->state); ?>

                            <div class="mg-b-25">
                                <span class="tx-gray-800 tx-uppercase tx-semibold tx-13 ">User Info</span>
                                <div class="question-button">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <label class="tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Name:-</label>
                                    <p class="tx-inverse mg-b-25" style="font-size: 15px;">{{ $user->first_name.' '.$user->last_name }}</p>
    
                                    <label class="tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Email:-</label>
                                    <p class="tx-inverse mg-b-25" style="font-size: 15px;">{{ $user->email }}</p>

                                    <label class="tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Phone No:-</label>
                                    <p class="tx-inverse mg-b-25" style="font-size: 15px;">{{$user->phone}}</p>  
                                </div>
                                <div class="col-lg-6"> 
                                    <label class="tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Address:- </label>
                                    <p class="tx-inverse mg-b-25" style="font-size: 15px;">{{ $user->street.', '.$city.', '.$state }}</p>
                                
                                    <label class="tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2"> Zip Code:-</label>
                                    <p class="tx-inverse mg-b-25" style="font-size: 15px;"
                                    >{{ $user->zip_code }}</p>
                                </div>
                            </div>
                        </div><!-- card -->
                    </div><!-- col-lg-4 -->
                    <div class="col-lg-12" style="margin-top: 40px;"> 
                        <div class="media-list bg-white rounded shadow-base">
                            <div class=" pd-20 pd-xs-30 ">
                                <div class="mg-b-25">
                                    <span class="tx-gray-800 tx-uppercase tx-semibold tx-13 ">User Category Skills</span>
                                    <div class="question-button">
                                     </div>
                                </div>
                                <div class="media-body"> 
                                        <?php if(count($category_data)){
                                            foreach($category_data as $cd){?>
                                                <table style="margin-top:30px">
                                                    <tr>
                                                        <th>Category </th>
                                                        <th colspan=2><p style="margin: 0px;text-align: center"><?=ucwords($cd->category_name)?></p></th>
                                                    </tr>
                                                <?php $customer_skills= DB::table('customer_skills')->select('skill_name','skill_scores')->where('category_id',$cd->category_id)->get();
                                                    $i=1;
                                                    if(count($customer_skills)){?> 
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Skill Name</th>
                                                            <th>Skill Scores</th>
                                                        <tr>
                                                    <?php foreach($customer_skills as $cs){?>
                                                            <tr>
                                                                <td><?=$i?></td>
                                                                <td><?= $cs->skill_name?> </td>
                                                                <td><?= $cs->skill_scores?></td>
                                                            <tr>
                                                <?php $i++; } } ?>
                                                    <tr>  
                                                        <th colspan=2>Total Scores</th>
                                                        <th><?= DB::table('customer_skills')->where('category_id',$cd->category_id)->groupBy('category_id')->sum('skill_scores');?></th>
                                                    </tr>
                                                <?php } ?> </table><?php }?> 
                                </div>

                            </div><!-- media -->
                        </div> 
                    </div>
                </div><!-- row -->
            </div><!-- tab-pane -->
        </div><!-- br-pagebody -->

    </div><!-- br-mainpanel -->
@endsection

