@extends('admin.layouts.app')
@section('content')
     <div class="br-pagetitle">
            <h4>Manage Users </h4>
    </div>

    <div class="br-pagebody"> 
        <div class="br-section-wrapper">
          <div class="row">
            <div class="col-lg-6">
                <h6 class="br-section-label">All Users</h6>
            </div> 
        </div>
            <div class=" rounded table-responsive mg-t-20">
                <table class="table table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th>S.No</th>
                        <th> Name</th>
                        <th> Phone No</th> 
                        <th>Email</th>
                        <th>Address</th>
                        <th>Zip Code</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sr = 1; 
                    if(!empty($users)){
                        foreach ($users as $row){
                            $city=CommonFunction::getSingleField('cities','city','id',$row->city);
                            $state=CommonFunction::getSingleField('states','name','id',$row->state); ?>
                            <tr>
                                <td scope="row">{{$sr}}</td> 
                                <td class="cr"> {{$row->first_name .' '.$row->last_name}}</td> 
                                <td class="cr">{{$row->phone}}</td>
                                 <td class="cr">{{$row->email}}</td> 
                                <td class="cr">{{$row->street.', '.$city.', '.$state}}</td>
                                 <td class="cr">{{$row->zip_code}}</td>
                                <td style="">
                                    <div style="display: flex"> 
                                         <a  data-toggle="tooltip" data-placement="top" title="View Customer!" href="{{url('admin/user-detail',$row->customer_id)}}" id="view"
                                           class="btn btn-info btn-icon mg-r-5 mg-b-10">
                                            <div><i id="edit" class="fa fa-eye"></i></div>
                                        </a>  
                                    </div>
                                </td>
                            </tr>
                        <?php $sr++; } } else{?>
                        <tr>
                            <td colspan="5">No Record Found</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="mg-t-20"></div>
        </div><!-- br-section-wrapper-->
    </div>
@endsection
