@extends('admin.layouts.app')
@section('content')
   <div class="br-pagetitle">
        <div>
            <h4>Dashboard</h4>
        </div>
    </div>

    <div class="br-pagebody">
        <div class="row row-sm">
            <div class="col-sm-6 col-xl-3">
                <a href="{{ url('admin/users')}}">
                    <div class="bg-info rounded overflow-hidden">
                        <div class="pd-x-20 pd-t-20 d-flex align-items-center"> 
                            <i class="fas fa-users tx-70 lh-0 tx-white op-10"></i>
                            <div class="mg-l-20">
                            <?php  $totalUserFormat=number_format($total_users);?>
                                <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Total Users</p>
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{$totalUserFormat}}</p>
                             </div>
                        </div>
                        <div id="ch1" class="ht-50 tr-y-1"></div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-xl-3">
                <a href="{{url('admin/category')}}">
                    <div class="bg-info rounded overflow-hidden">
                        <div class="pd-x-20 pd-t-20 d-flex align-items-center"> 
                            <i class="fab fa-cuttlefish tx-70 lh-0 tx-white op-10"></i>
                            <div class="mg-l-20">
                            <?php  $totalCategoryFormat=number_format($total_categories);?>
                                <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Total Categories</p>
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{$totalCategoryFormat}}</p>
                             </div>
                        </div>
                        <div id="ch1" class="ht-50 tr-y-1"></div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-xl-3">
                <a href="{{ url('admin/subcategory')}}">
                    <div class="bg-info rounded overflow-hidden">
                        <div class="pd-x-20 pd-t-20 d-flex align-items-center"> 
                            <i class="fab fa-stripe-s tx-70 lh-0 tx-white op-10"></i>
                            <div class="mg-l-20">
                            <?php  $totalSubcategoryFormat=number_format($total_subcategories);?>
                                <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Total Subcategories</p>
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{$totalSubcategoryFormat}}</p>
                             </div>
                        </div>
                        <div id="ch1" class="ht-50 tr-y-1"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection