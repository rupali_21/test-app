@extends('admin.layouts.app')
@section('content')
     <div class="br-pagetitle">
            <h4>Manage Categories </h4>
    </div>

    <div class="br-pagebody">
         @extends('admin.layouts.messages')
        <div class="br-section-wrapper">
          <div class="row">
            <div class="col-lg-6">
                <h6 class="br-section-label">All Categories</h6>
            </div>
        </div>
            <div class=" rounded table-responsive mg-t-20">
                <table class="table table-bordered mg-b-0">
                    <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Category Name</th> 
                        <th>Status</th> 
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sr = 1; 
                    if(!empty($category)){
                        foreach ($category as $row){?>
                            <tr>
                                <td scope="row">{{$sr}}</td> 
                                <td class="cr">{{ucwords($row->category_name)}}</td> 
                                <td class="cr">
                                      <?php if($row->status==1){?>
                                       Active
                                        <?php }else if($row->status ==0){?> 
                                        Inavtive
                                         <?php }?>
                                </td> 
                            </tr>
                        <?php $sr++; }} else{ ?>
                            <tr>
                                <td colspan="5">No Record Found</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="mg-t-20"></div>
        </div><!-- br-section-wrapper-->
    </div>
@endsection
