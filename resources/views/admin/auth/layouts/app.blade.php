<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$title}} -Test App</title>
    <!-- vendor css -->
 <link rel="shortcut icon" href="{{asset('public/assets/admin/img/froiden-logo.png')}}"/>
    <link href="{{ asset('public/assets/admin/lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/admin/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
 
    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{ asset('public/assets/admin/css/bracket.css') }}">
</head>
<body>
@yield('content')
</body>
</html>
<script src="{{ asset('public/assets/admin/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/jquery-ui/ui/widgets/datepicker.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
