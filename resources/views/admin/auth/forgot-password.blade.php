@extends('admin.auth.layouts.app')
@section('content')
<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v " style="background-color:#117a8b">

    <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
        <div class="signin-logo tx-center tx-28 tx-bold tx-inverse "><img src="{{url('public/assets/admin/img/logo-froiden.png') }}" style="width:90px;height:30px"></div>
        <div class="tx-center mg-b-40"></div>
        @if(Session::has('success'))
            <div class="alert alert-success {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ Session::get('success') }}
            </div>
        @endif
          @if(Session::has('error'))
            <div class="alert alert-danger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ Session::get('error') }}
            </div>
        @endif
        <form method="post" action="{{ url('admin/forgot-password') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="text" class="form-control{{ $errors->has('email') ? ' border border-danger' : '' }}" value="{{ old('email') }}" name="email" id="email" placeholder="Enter your Email">
                @if ($errors->has('email'))
                    <span class="text-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif
            </div><!-- form-group -->
            <button type="submit" name="submit" class="btn btn-info btn-block">Submit</button>
        </form>

        <a href="{{ url('admin/login') }}" class="tx-info text-center tx-12 d-block mg-t-10">Back to Login</a>

    </div><!-- login-wrapper -->
</div><!-- d-flex -->
@endsection