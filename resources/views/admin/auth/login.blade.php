@extends('admin.auth.layouts.app')
@section('content')
    <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v" style="background-color:#117a8b">

        <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
            <div class="signin-logo tx-center tx-28 tx-bold tx-inverse"><img src="{{url('public/assets/admin/img/logo-froiden.png') }}" style="width:90px;height:30px"></div>
            <div class="tx-center mg-b-40"></div>
            @if(Session::has('error'))
                <div class="alert alert-warning {{ Session::get('alert-class', 'alert-info') }}">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('error') }}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success {{ Session::get('alert-class', 'alert-info') }}">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('success') }}
                </div>
            @endif
            <form method="POST" action="{{ url('admin/login') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <input type="text" name="username" value="<?php if(old('username')){{old('username');}} ?>" class="form-control" placeholder="Enter your username">
                    @if ($errors->has('username'))
                        <span class="help-block text-danger">
                         {{ $errors->first('username') }}
                        </span>
                    @endif
                </div><!-- form-group -->
                <div class="form-group">
                    <input type="password" name="password" value="<?php if(old('password')){{old('password');}} ?>"   class="form-control" placeholder="Enter your password">
                    @if ($errors->has('password'))
                        <span class="help-block text-danger">
                           {{ $errors->first('password') }}
                        </span>
                    @endif
 
                </div><!-- form-group -->
                <button type="submit" name="submit" class="btn btn-info btn-block">Sign In</button>
            </form>
            <a href="{{ url('admin/forgot-password') }}" class="tx-info text-center tx-12 d-block mg-t-10">Forgot password?</a>

        </div><!-- login-wrapper -->
    </div><!-- d-flex -->
@endsection