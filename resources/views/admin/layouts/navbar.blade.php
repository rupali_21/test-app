
   
    <div class="br-header">
    <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
    </div><!-- br-header-left -->
    <div class="br-header-right">
        <nav class="nav">
            <div class="dropdown">
                    <div class="dropdown-menu-label"> 
                     </div> 
                <div class="dropdown-menu dropdown-menu-header">
                    <div class="dropdown-menu-label">
                        <label>Notifications</label>
                       
                            <a href="">Mark All as Read</a>
                

                    </div><!-- d-flex -->

                    <div class="media-list">
                   
                            <a href="" class="media-list-link read">
                                <div   style="background-color: #cccccc;" class="media">
                                    <div  class="media-body">
                                            <p class="noti-text"></p>
                                            <span>
                                           
                                        </span>
                                </div>
                                </div><!-- media -->
                            </a>
                     
                        <div class="dropdown-footer">
                            <a href=""><i class="fas fa-angle-down"></i> Show All Notifications</a>
                        </div>
                    </div><!-- media-list -->
                </div><!-- dropdown-menu -->
            </div>

            <div class="dropdown">
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down"></span> 
                    <img src="{{ asset('public/admin/'.Auth::user()->profile_image) }}" class="wd-32 rounded-circle" alt="">
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-250">
                    <ul class="list-unstyled user-profile-nav">
                        <li><a href="{{url('admin/my-profile')}}"><i class="far fa-user"></i> My Profile</a></li>
                        <li><a href="{{url('admin/change-password')}}"><i class="icon ion-key"></i> Change Password</a></li>
                        <li><a href="{{url('admin/logout')}}"><i class="icon ion-power"></i> Sign Out</a></li>
                    </ul>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </nav>
    </div><!-- br-header-right -->
</div>