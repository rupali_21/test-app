<!-- ########## START: LEFT PANEL ########## -->

<?php
    $currentPage = Request::segment(2);
    /*$currentPage = '';
    $url = url()->current();
    if($url != ''){
        $activeURL = explode("/", $url);
        if(isset($activeURL[5]))
            $currentPage = $activeURL[5];
    }*/
?>


<div class="br-logo"><a href="{{ url('admin/dashboard')}}"><img src="{{url('public/assets/admin/img/logo-froiden.png') }}"  style="width:90px;height:30px"></a></div>
<div class="br-sideleft sideleft-scrollbar">
    <!-- <label class="sidebar-label pd-x-10 mg-t-20 op-3">Navigation</label> -->
    <ul class="br-sideleft-menu"> 
        <li class="br-menu-item">
            <a href="{{ url('admin/dashboard')}}" class="br-menu-link {{ ($currentPage == '/') ? 'active': '' }}">
                <i class="fa fa-tachometer-alt"></i>
                <span class="menu-item-label">Dashboard </span>
            </a>
        </li> 

        <li class="br-menu-item">
            <a href="{{ url('admin/users')}}" class="br-menu-link  {{ ($currentPage == 'users') ? 'active': '' }}">
                <i class="fas fa-users"></i>
                <span class="menu-item-label">Users</span>
            </a>
        </li> 
       
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub {{ ($currentPage == 'category' || $currentPage == 'subcategory') ? 'active': '' }}">
            <i class="fab fa-cuttlefish"></i>
            <span class="menu-item-label">Category Management</span>
            </a>
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="{{ url('admin/category')}}" class="sub-link">Category</a></li>
                <li class="sub-item"><a href="{{ url('admin/subcategory')}}" class="sub-link">Subcategory</a></li>
            </ul>
        </li>

    </ul>

    <br>
</div>
<!-- ########## END: LEFT PANEL ########## -->