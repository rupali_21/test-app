<footer class="br-footer">
    <div class="footer-left">
        <div class="mg-b-2">Copyright &copy; {{ date('Y') }}. Test App. All Rights Reserved.</div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('input[type="file"]').change(function(e){
                var fileName = e.target.files[0].name;
                $('.custom-file-label').text(fileName);
            });
        });
    </script>
<!-- 
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script> -->
</footer>