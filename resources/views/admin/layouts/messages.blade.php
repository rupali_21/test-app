<!-- @if(Session::has('success'))
    <div class="alert alert-success {{ Session::get('alert-class') }}"
    style="padding:1.5%;">
      {{Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif

@if (Session::has('error'))
    <div class="alert alert-danger {{ Session::get('alert-class') }}" 
    style="padding:1.5%;">
        {{Session::get('error')}}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif  -->

@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif
    @if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif