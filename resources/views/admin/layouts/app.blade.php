<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$title}} - Test App</title>
    <link href="{{ asset('public/assets/admin/lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/admin/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/admin/lib/highlightjs/styles/github.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/admin/lib/timepicker/jquery.timepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/admin/lib/spectrum-colorpicker/spectrum.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/admin/lib/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/admin/lib/ion-rangeslider/css/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/admin/lib/ion-rangeslider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">    
    <link href="{{ asset('public/assets/admin/lib/medium-editor/css/medium-editor.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/admin/lib/medium-editor/css/themes/default.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/admin/lib/summernote/summernote-bs4.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('public/assets/admin/lib/jquery/jquery-1.7.1.min.js')}}" ></script>
    <link rel="stylesheet" href="{{ asset('public/assets/user/bootstrap-3/css/main-css.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/admin/lib/datatables.net-dt/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/admin/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{ asset('public/assets/admin/css/bracket.css') }}">
     <!--main css-->
    <!--  -->
    <!-- Custom-->
    <link rel="stylesheet" href="{{ asset('public/assets/admin/css/custom.css') }}">
 <link rel="shortcut icon" href="{{asset('public/assets/admin/img/froiden-logo.png')}}"/>
 
    <!--Cropping CSS-->
    <link rel="stylesheet" href="{{ asset('public/assets/cropping/croppie.min.css') }}">

    <script src="{{ asset('public/assets/admin/lib/jquery/jquery.min.js') }}"></script>-
    
    <script src="{{ asset('public/assets/admin/js/jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/cropping/croppie.js') }}"></script>

    {{--<script type="text/javascript" src="{{ asset('public/assets/admin/css/datatable.js')}}" ></script>--}}

    <script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
    <script type="text/javascript">
       
    </script>
  
</head>

<body>

    @include('admin.layouts.sidebar')
    @include('admin.layouts.navbar')
    <div class="br-mainpanel">
        @yield('content')
        @include('admin.layouts.footer')
    </div>
{{--<script>--}}
    {{--$('.table').DataTable( {--}}
    {{--});--}}
{{--</script>--}}

<!-- Crop Image from CI -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.37.0/jquery.form.js"></script>

 
<script src="{{ asset('public/assets/admin/lib/jquery-ui/ui/widgets/datepicker.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/highlightjs/highlight.pack.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/timepicker/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/spectrum-colorpicker/spectrum.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/jquery.maskedinput/jquery.maskedinput.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/lib/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>


    <script src="{{ asset('public/assets/admin/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/assets/admin/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/assets/admin/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('public/assets/admin/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>

<script src="{{ asset('public/assets/admin/js/bracket.js') }}"></script>
    <script>
        $(function(){
            'use strict';

            $('.table').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            $('#datatable2').DataTable({
                bLengthChange: false,
                searching: false,
                responsive: true
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
<script type="text/javascript">

    $(function() {
        'use strict'

        // Inline editor
        //var editor = new MediumEditor('.editable');
        // Summernote editor
        /*$('#summernote').summernote({
            height: 150,
            tooltip: false
        })*/

        // Toggles
        $('.br-toggle').on('click', function (e) {
            e.preventDefault();
            $(this).toggleClass('on');
        });

        // Input Masks
        $('#dateMask').mask('99/99/9999');
        $('#phoneMask').mask('(999) 999-9999');
        $('#ssnMask').mask('999-99-9999');
        var date = new Date();
        date.setDate(date.getDate()-1);
        // Datepicker
        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            numberOfMonths: 1,
            dateFormat: 'dd-mm-yy',
            minDate:new Date()
            //minDate: 'dateToday'
        });


        $(function () {
            'use strict'

            // Toggles
            $('.br-toggle').on('click', function (e) {
                e.preventDefault();
                $(this).toggleClass('on');
            });

            //Datepicker
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true
            });

            // Time Picker
            $('#setTimeButton').on('click', function () {
                $('#tp3').timepicker('setTime', new Date());
            });


            // Rangeslider
            if ($().ionRangeSlider) {
                $('#rangeslider1').ionRangeSlider();

                $('#rangeslider2').ionRangeSlider({
                    min: 100,
                    max: 1000,
                    from: 550
                });

                $('#rangeslider3').ionRangeSlider({
                    type: 'double',
                    grid: true,
                    min: 0,
                    max: 1000,
                    from: 200,
                    to: 800,
                    prefix: '$'
                });

                $('#rangeslider4').ionRangeSlider({
                    type: 'double',
                    grid: true,
                    min: -1000,
                    max: 1000,
                    from: -500,
                    to: 500,
                    step: 250
                });
            }

        });
    });

    $(function(){
        // showing modal with effect
        $('.modal-effect').on('click', function(e){
          e.preventDefault();

          var effect = $(this).attr('data-effect');
          $('#modaldemo8').addClass(effect);
          $('#modaldemo8').modal('show');
        });

        // hide modal with effect
        $('#modaldemo8').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });
    });

    //ckeditor
    CKEDITOR.replace( 'description' );

    
    </script>
    <script>
        //alert hide after some time
        $(document).ready(function(){
            setInterval(function(){ $("div.alert").hide(); }, 5000)
        });
    </script>

</body>
</html>
