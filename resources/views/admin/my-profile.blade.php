@extends('admin.layouts.app')
@section('content')
    <div class="br-pagetitle">
        <h4> Manage Profile</h4>
    </div>
    <div class="br-pagebody"> 
        <div class="br-section-wrapper">
            <?php if(Session::has('success')){?>
                <div class="alert alert-success {{ Session::get('alert-class') }}">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('success') }}
                </div>
            <?php } if(Session::has('error')){ ?>
                <div class="alert alert-danger {{ Session::get('alert-class') }}">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('error') }}
                </div> 
            <?php } ?> 
            <h6 class="br-section-label">My Profile</h6>
            <form method="post" action="{{ url('admin/my-profile') }}" enctype="multipart/form-data">
                {{ csrf_field() }}    
                <div class="row mg-t-20">
                    <div class="col-lg-6">
                        <label class="text-form">Username<sup> <span class="text-danger">*</span> </sup></label>
                        <input class="form-control {{ $errors->has('user_name') ? ' border border-danger' : '' }}"
                               value="<?= $profile->user_name ?>" id="user_name" name="user_name"
                               type="text">
                        @if ($errors->has('user_name'))
                            <span class="text-danger">
                                   {{ $errors->first('user_name') }}
                                </span>
                        @endif
                    </div><!-- col -->
                     <div class="col-lg-6">
                        <label class="text-form">Email<sup> <span class="text-danger">*</span> </sup></label>
                        <input class="form-control {{ $errors->has('email') ? ' border border-danger' : '' }}"
                               value="<?= $profile->email ?>" id="email" name="email"
                               type="text">
                        @if ($errors->has('email'))
                            <span class="text-danger">
                                   {{ $errors->first('email') }}
                                </span>
                        @endif
                    </div><!-- col --> 
                </div><!-- row -->

                <div class="row mg-t-20">
                    <div class="col-lg-6">
                        <label class="text-form">Mobile Number<sup> <span class="text-danger">*</span> </sup></label>
                        <input class="form-control {{ $errors->has('mobile_number') ? ' border border-danger' : '' }}"
                               value="<?= $profile->mobile_number ?>" id="mobile_number" name="mobile_number"
                               type="text">
                        @if ($errors->has('mobile_number'))
                            <span class="text-danger">
                                   {{ $errors->first('mobile_number') }}
                                </span>
                        @endif
                    </div><!-- col -->
                     <div class="col-lg-6">
                        <label class="text-form">Profile Image<sup> <span class="text-danger">*</span> </sup></label>
                        <input class="form-control {{ $errors->has('profile_image') ? ' border border-danger' : '' }}"
                               value="<?= $profile->profile_image ?>" id="profile_image" name="profile_image"
                               type="file">
                        <?php if($profile->profile_image != ""){?>
                            <img src="{{ asset('public/admin/'.$profile->profile_image) }}" alt="">
                        <?php } ?>
                        @if ($errors->has('profile_image'))
                            <span class="text-danger">
                                   {{ $errors->first('profile_image') }}
                                </span>
                        @endif
                    </div><!-- col --> 
                </div><!-- row -->
                <div class="row mg-t-30">
                    <div class="col-sm-6 col-md-2">
                        <div class="btn-demo">
                            <button type="submit" name="submit" class="btn btn-primary btn-block mg-b-10">Save</button>
                        </div><!-- btn-demo -->
                    </div><!-- col-sm-3 -->

                    <div class="col-sm-6 col-md-2 mg-t-20 mg-sm-t-0">
                        <div class="btn-demo">
                            <a href="{{ url('admin/my-profile') }}">
                                <button type="button" class="btn btn-secondary btn-block mg-b-10">Cancel</button>
                            </a>
                        </div><!-- btn-demo -->
                    </div><!-- col-sm-3 -->
                </div>
            </form>
        </div>
    </div>
@endsection