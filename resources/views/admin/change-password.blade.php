@extends('admin.layouts.app')
@section('content')
    <div class="br-pagetitle">
        <h4>Change Password</h4>
    </div>
    <div class="br-pagebody"> 
        <div class="br-section-wrapper">
            <?php if(Session::has('success')){?>
                <div class="alert alert-success {{ Session::get('alert-class') }}">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('success') }}
                </div>
            <?php } if(Session::has('error')){ ?>
                <div class="alert alert-danger {{ Session::get('alert-class') }}">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('error') }}
                </div> 
            <?php } ?>

            <h6 class="br-section-label" style="margin-top:10px">Change account password</h6>
            <form method="post" action="{{ url('admin/change-password') }}">
                {{ csrf_field() }}
                <div class="row mg-t-20">
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-6">
                        <label class="text-form">Current Password<sup> <span class="text-danger">*</span> </sup></label>
                        <input class="form-control {{ $errors->has('current_password') ? ' border border-danger' : '' }}"
                               value="{{ old('current_password') }}" id="current_password" name="current_password"
                               type="password">
                        @if ($errors->has('current_password'))
                            <span class="text-danger">
                                   {{ $errors->first('current_password') }}
                                </span>
                        @endif
                    </div><!-- col -->

                </div><!-- row -->

                <div class="row mg-t-20">
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-6 mg-t-10 mg-lg-t-0">
                        <label class="text-form">New Password<sup> <span class="text-danger">*</span> </sup></label>
                        <input type="password"
                               class="form-control{{ $errors->has('new_password') ? ' border border-danger' : '' }}"
                               id="new_password" value="{{ old('new_password') }}" name="new_password">
                        @if ($errors->has('new_password'))
                            <span class="text-danger">
                                    {{ $errors->first('new_password') }}
                                </span>
                        @endif
                    </div>

                </div>
                <div class="row mg-t-20">
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-6 mg-t-10 mg-lg-t-0">
                        <label class="text-form">Confirm Password<sup> <span class="text-danger">*</span> </sup></label>
                        <input type="password"
                               class="form-control{{ $errors->has('confirm_password') ? ' border border-danger' : '' }}"
                               id="confirm_password" value="{{ old('confirm_password') }}" name="confirm_password">
                        @if ($errors->has('confirm_password'))
                            <span class="text-danger">
                                   {{ $errors->first('confirm_password') }}
                                </span>
                        @endif
                    </div>

                </div>
                <div class="row mg-t-30">
                    <div class="col-lg-3">
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="btn-demo">
                            <button type="submit" name="submit" class="btn btn-primary btn-block mg-b-10">Save</button>
                        </div><!-- btn-demo -->
                    </div><!-- col-sm-3 -->

                    <div class="col-sm-6 col-md-2 mg-t-20 mg-sm-t-0">
                        <div class="btn-demo">
                            <a href="{{ url('admin/change-password') }}">
                                <button type="button" class="btn btn-secondary btn-block mg-b-10">Cancel</button>
                            </a>
                        </div><!-- btn-demo -->
                    </div><!-- col-sm-3 -->
                </div>
            </form>
        </div>
    </div>
@endsection