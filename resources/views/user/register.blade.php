@extends('user.layouts.app')
@section('content')

<style> 
    .multiselect{
        width: 100% !important;
    } 
    .btn-group{width: 100% !important;}
    .show{
            width: 100% !important;
    }
    .caret{
        display: none;
    }
</style> 

    <div class="br-pagetitle">
            <h4> Register</h4>
    </div>

    <div class="br-pagebody">
        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger">
                {{session('error')}}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
        @endif
        <form action="{{url('register')}}" method="post" id="store" enctype="multipart/form-data">
            {{csrf_field()}}

            <div class="br-section-wrapper"> 
                <div class="Details">
                    <!-- <h5>Add School </h5> --> 
                    <div class="row mg-t-20">
                        <div class="col-lg-4">
                            <label class="text-form">First Name<sup> <span class="text-danger">*</span> </sup></label>
                            <input id="first_name" maxlength="30" value="{{old('first_name')}}" name="first_name" class="form-control {{ $errors->has('first_name') ? ' border border-danger' : '' }}"  type="text">
                            @if ($errors->has('first_name'))
                                <span class="text-danger">
                                    {{ $errors->first('first_name') }}
                                    </span>
                            @endif
                        </div>
                        <div class="col-lg-4">
                            <label class="text-form">Last Name<sup> <span class="text-danger">*</span> </sup></label>
                            <input id="last_name" maxlength="30" value="{{old('last_name')}}" name="last_name" class="form-control {{ $errors->has('last_name') ? ' border border-danger' : '' }}"  type="text">
                            @if ($errors->has('last_name'))
                                <span class="text-danger">
                                    {{ $errors->first('last_name') }}
                                    </span>
                            @endif
                        </div>
                        <div class="col-lg-4">
                            <label class="text-form">Phone No<sup> <span class="text-danger">*</span> </sup></label>
                            <input id="phone" maxlength="10" value="{{old('phone')}}" name="phone" class="form-control {{ $errors->has('phone') ? ' border border-danger' : '' }}"  type="text">
                            @if ($errors->has('phone'))
                                <span class="text-danger">
                                    {{ $errors->first('phone') }}
                                    </span>
                            @endif
                        </div> 
                    </div>

                    <div class="clearfix"></div>
        
                    <div class="row mg-t-20"> 
                        <div class="col-lg-4">
                            <label class="text-form">Street<sup> <span class="text-danger">*</span> </sup></label>
                            <input id="street" maxlength="100" value="{{ old('street')}}" name="street" class="form-control {{ $errors->has('street') ? ' border border-danger' : '' }}"  type="text">
                            @if ($errors->has('street'))
                                <span class="text-danger">
                                    {{ $errors->first('street') }}
                                    </span>
                            @endif
                        </div> 
                        <div class="col-lg-4">
                            <label class="text-form"> State <sup> <span class="text-danger">*</span> </sup></label>
                            <select onchange="GetCity(this.value,'');" class="form-control" id="state_id" name="state_id">
                                <option value=""> Choose State</option>
                                @foreach($state as $row)
                                        @if (old('state_id') == $row->id)
                                            <option selected value="{{ $row->id }}">{{ ucwords(strtolower($row->name)) }}</option>
                                        @else
                                            <option value="{{ $row->id }}">{{ ucwords(strtolower($row->name)) }}</option>
                                        @endif
                                @endforeach
                            </select>
                            @if ($errors->has('state_id'))
                                <span class="text-danger">
                                    {{ $errors->first('state_id') }}
                                </span>
                            @endif
                        </div>
                        <div class="col-lg-4">
                            <label class="text-form">City<sup> <span class="text-danger">*</span> </sup></label>
                            <select id="city_id" name="city_id" class="form-control {{ $errors->has('city') ? ' border border-danger' : '' }}" data-placeholder="Choose on">
                                <option value="">Select State First</option>
                            </select>
                            @if ($errors->has('city_id'))
                                <span class="text-danger">
                                        {{ $errors->first('city_id') }}
                                    </span>
                            @endif
                        </div>
                        <div class="clearfix"></div> 
                        <div class="col-lg-4 mg-t-20">
                            <label class="text-form">Email <sup> <span class="text-danger">*</span> </sup></label>
                            <input id="email" maxlength="100" value="{{old('email')}}" name="email" class="form-control {{ $errors->has('email') ? ' border border-danger' : '' }}"  type="text">
                            @if ($errors->has('email'))
                                <span class="text-danger">
                                    {{ $errors->first('email') }}
                                    </span>
                            @endif
                        </div>
                        <div class="col-lg-4 mg-t-20">
                            <label class="text-form">Zip<sup> <span class="text-danger">*</span> </sup></label>
                            <input id="zip" maxlength="7" value="{{ old('zip')}}" name="zip" class="form-control {{ $errors->has('zip') ? ' border border-danger' : '' }}"  type="number">
                            @if ($errors->has('zip'))
                                <span class="text-danger">
                                    {{ $errors->first('zip') }}
                                    </span>
                            @endif
                        </div>  
                    </div>

                    <div class="clearfix"></div>

                    <div class="row mg-t-20"> 
                        <div class="col-lg-4">
                            <div class="col-lg-12" style="padding: 0px;">
                                <label class="text-form"> Category <sup> <span class="text-danger">*</span> </sup></label>
                            </div>
                            <div class="col-lg-12" style="padding: 0px;">
                                <select onchange="Getcategory();" id="category_id" name="category_id[]"
                                    multiple="multiple"
                                    class="form-control select2  elect2-hidden-accessible chk_mul"
                                    style="width: 100%;" tabindex="-1"
                                    aria-hidden="true">
                                    <option value=""> Choose Category</option>
                                    @foreach($category as $row)
                                        <option value="{{ $row->category_id }}">{{ ucwords(strtolower($row->category_name)) }}</option>
                                    @endforeach
                                </select>
                            </div> 
                            <span class="text text-danger cat-error">  </span>
                            @if ($errors->has('category_id'))
                                <span class="text-danger">
                                    {{ $errors->first('category_id') }}
                                </span>
                            @endif
                        </div>
                        <div class="col-lg-8" >
                            <div class="col-lg-12" style="padding: 0px;">
                                <label class="text-form lable"> Skills <sup> <span class="text-danger">*</span> </sup></label>
                            </div>
                            <div class="col-lg-12" id="skill" style="padding: 0px;">
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row mg-t-30">
                        <div class="col-sm-6 col-md-2">
                            <div class="btn-demo">
                                <button name="submit" type="submit" class="btn btn-primary submit btn-block mg-b-10">Save</button>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-2 mg-t-20 mg-sm-t-0">
                            <div class="btn-demo">
                                <a href=""><button type="button" class="btn btn-secondary btn-block mg-b-10">Cancel</button></a>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </form>
    </div>

    <script>   
        $(document).ready(function(){
            $('.lable').hide();
            $('#category_id option:checked').each(function(){
            $(this).prop('checked', true);
            });
        });

        $(".submit").click(function(){ 
            var category_id= $("#category_id").val();
            var skill_score=$("#skill_score").val(); 
            var data=true;
            if(category_id==''){
                $(".cat-error").text('please select category')
                data =false; 
            } 
            return data;  
        });
        
        function Getcategory(){ 
            var category_id= $("#category_id").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url:'{{ url('skills') }}',
                data: {selectedCategory : category_id},
                dataType: "json",
                success: function(response) { 
                    $('.lable').show();
                    var category_data = response.data; 
                    var category_name = response.cat_name; 
                    var option_string_default = '';
                    var option_string =''; 
                    $.each(category_data, function(i, e) {   
                        $.each(e, function(k, v){
                            option_string += '<div class="col-lg-6"><input class="form-control" type="hidden" name="skill_id[]" value="' + v.skill_id	+ '"></div>';
                            option_string += '<div class="col-lg-6"><input class="form-control" type="hidden" name="parent_id[]" value="' + v.parent_id	+ '"></div> ';
                            option_string += '<div class="col-lg-6"><input class="form-control" type="text" name="category_name[]" value="' + v.category_name + '" readonly></div>';
                            option_string += '<div class="col-lg-6"><input class="form-control" min="0" max="5" id="skill_score" type="number" name="skill_score[]" required></div> '; 
                        }); 
                    });
                    option_string +=' ';
                    $("#skill").html('').append(option_string_default + ' ' + option_string);
                }
            });
        }

        function GetCity(state_id,oldcity) { 
            var $this=$(this); 
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url:'{{ url('city') }}',
                data: {state_id : state_id},
                dataType: "json",
                success: function(response) {

                    var state_data = response;
                    //console.log(category_data);
                    var option_string_default = '';
                    var option_string = '<option value="">Select City</option>';
                    $.each(state_data, function(i, e) {
                        if(oldcity ==e.id ){
                            option_string += '<option selected selected ';
                            option_string += 'value="' + e.id	+ '" ';
                            option_string += '>';
                            option_string += e.city;
                            option_string += '</option>';
                        }else{
                            option_string += '<option ';
                            option_string += 'value="' + e.id	+ '" ';
                            option_string += '>';
                            option_string += e.city;
                            option_string += '</option>';
                        } 
                    });
                    $("#city_id").html('').append(option_string_default + ' ' + option_string);
                }
            });
        }
        
        $( document ).ready(function() {
            var state_id = $('#state_id').val();
            var oldcity='{{ old('city_id') }}';
            if(state_id){
                if(oldcity){
                    GetCity(state_id,oldcity);
                }
                else{
                    GetCity(state_id,'');
                }
            }
        });
    </script>
 
@endsection