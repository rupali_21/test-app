@extends('user.layouts.app')
@section('content')
    <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v" style="background-color:#117a8b">
    @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
    @endif
        @if(session('error'))
        <div class="alert alert-danger">
            {{session('error')}}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
    @endif
    <h1>
        <a href="" class="typewrite" data-period="4000" data-type='[ "WELCOME TO THE FROIDEN TECHNOLOGY PVT. LTD.", "WE PROVIDE GREAT SERVICES.","MAKE CARRER IN DIFFERENT FIELDS & TECHNOLOGIES IN FROIDEN."]'>
            <span class="wrap"></span>
        </a>
    </h1>
                
    </div><!-- d-flex -->
@endsection