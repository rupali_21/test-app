<?php

namespace App\Http\Controllers\Admin;
 
use App\Http\Controllers\Controller;
use CommonFunction;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Auth;
use Validator;  
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotMail;
use Password; 
use DB; 

class LoginController extends Controller
{
    public $successStatus = 200;
    use SendsPasswordResetEmails;

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    private $entryDate;
    public function __construct()
    { 
        $this->middleware('guest:admin')->except('logout');
         $this->entryDate = date("Y-m-d H:i:s");
    }

    
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */

    

    // This function is used for forgot password
    public function forgotPassword(Request $request){
        $data['title']='Forgot Password';
        if(isset($_POST['submit'])){
            $validator = Validator::make($request->all(), [  
                'email'=>'required'
            ]);
            if($validator->fails()){ 
                $request->session()->flash('error','Validation error occurred');
                return redirect()->back()->withInput(); 
            }
            
            $user = DB::table('users')->where(['email' => $request->email,'user_type'=>'Admin'])->first();
            
            if(!empty($user)){
                $password = mt_rand(100000, 999999);

                //send mail
                $data=array(
                        'name' => $user->user_name,
                        'email' => $user->email,
                        'password'=>$password
                    );
                
                Mail::to($user->email)->send(new ForgotMail($data));
                
                //Update the new password
                DB::table('users')->where(['email' => $user->email, 'user_type' => 'Admin'])
                    ->update(['password' => Hash::make($password), 'updated_at'=> date('Y-m-d H:i:s')]);
                $data = array('password' => $password);

                $request->session()->flash('success','New password has bee sent to your registered email address. Please check your inbox');
                return redirect('admin/login');
 
            }
            else{
                $request->session()->flash('error',"We couldn't find your account associated with this email");
                return redirect()->back()->withInput();
            }
        }else{
            return view('admin.auth.forgot-password',$data);
        }
    }
    
     public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect("/admin/login");
    }

    
    
}
