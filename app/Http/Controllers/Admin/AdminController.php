<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helper\CommonFunction; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Rules\AdminChangePassword;
use Validator;   
use App\Admin;  
use Mail;
use Illuminate\Support\Facades\Redirect;
use Password; 
use DB; 

class AdminController extends Controller
{
     private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
         $this->entryDate = date("Y-m-d H:i:s");
    }

    //function to get dashboard data
    public function dashboard(Request $request){
        $data['total_users']=DB::table('customers')->select('customer_id')->count();
        $data['total_categories']=DB::table('category')->select('category_id')->where(['parent_id'=>0])->count();
        $data['total_subcategories']=DB::table('category')->select('category_id')->where('parent_id','!=',0)->count();

        $data['title']='Dashboard';
        return view('admin.index',$data);
    } 
    
     //change password functionality
    public function changePassword(Request $request)
    {
        // print_r(Auth::user());exit;
         $data['title']='Change Password';
        if(isset($_POST['submit'])){
            extract($_POST);
            $rules=[
                'current_password' => ['required', new AdminChangePassword],
                'new_password' => 'required',
                'confirm_password' => ['required','same:new_password'],
            ];
            $customMessages = [ ];
            $this->validate($request, $rules, $customMessages); 

            if($request->new_password == $request->confirm_password){
                if($request->current_password != $request->new_password){
                        $data=array(
                            'password' =>Hash::make($request->new_password),
                            'updated_at'=>$this->entryDate,
                        );
                        $update= DB::table('users')
                            ->where('id', Auth::user()->id )
                            ->update($data); 
                    if ($update) {
                            \Session::flash('success', 'Your password has been changed successfully.');
                        return Redirect::back();
                    } else {
                            \Session::flash('error', 'Something went wrong.');
                        return Redirect::back(); 
                    }
                } 
                else{
                        \Session::flash('error', 'New password should be different from old password.');
                        return Redirect::back(); 
                }
            }else{
                \Session::flash('error', 'New password and confirm password must be same.');
                return Redirect::back();  
            }           
        }else{ 
            return view('admin.change-password',$data);
        }
    }

    //admin profile function
    public function myProfile(Request $request){
        $data['title']="My Profile";
        if(isset($_POST['submit'])){
            //functionality to edit admin profile 
            extract($_POST);
            $rules=[
                'user_name' =>'required|regex:/^[a-zA-Z\s]+$/|min:2|max:50', 
                'mobile_number' => 'required|regex:/^[0-9]+$/|min:10|max:10',
                'email' => 'required|email|min:2|max:70',
                'profile_image'=>'image|mimes:gif,jpg,png,bmp,jpeg,svg',
            ];
            $customMessages = [  
                'user_name.required'=>'Username must not be empty',
                'user_name.regex'=>'Username must be alphabet only',
                'user_name.min'=>'Username must be greater than 2 characters',
                'user_name.max'=>'Username must be less than 50 characters',
                'email.required'=>'Email must not be empty',
                'email.email'=>'Email must be in valid format',
                'email.min'=>'Email must be greater than 2 characters',
                'email.max'=>'Email must be less than 70 characters',
                'mobile_number.required'=>'Phone no must not be empty',
                'mobile_number.regex'=>'Phone no must be digits only',
                'mobile_number.min'=>'Phone no must be 10 digits only',
                'mobile_number.max'=>'Phone no must be 10 digits only',
                'profile_image.mimes'=>'Image must be gif,jpg,png,bmp,jpeg,svg',

            ];
            $this->validate($request, $rules, $customMessages);

            $data=array(
                'user_name'=>$user_name, 
                'mobile_number'=>$mobile_number,
                'email'=>$email,
            ); 
            $file = $request->file("profile_image"); 
            if ($request->hasFile("profile_image")) {
                $ext = $file->getClientOriginalExtension();
                $img = time() . md5($file->getClientOriginalName()) . "." . $ext;
                $file->move("public/admin/", $img); //Path & File name need to pass
                $data['profile_image'] = $img; //$file->getClientOriginalName();
            } 
            DB::table('users')->where('id',Auth::user()->id)->update($data); 

             $request->session()->flash('success', 'Your profile updated successfully.');
            return redirect()->back();
        }else{
            //get profile form
            $data['profile']=DB::table('users')->where('id',Auth::user()->id)->first();
            return view('admin.my-profile',$data);
        } 
    }
    
}
