<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helper\CommonFunction; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;   
use App\Admin;  
use Mail;
use Password; 
use DB; 

class UserController extends Controller
{
     private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
         $this->entryDate = date("Y-m-d H:i:s");
    }

    //function to get user details
    public function userDetail(Request $request,$id){  
        $data['user']=DB::table('customers')->where('customer_id',$id)->first();
        $data['category_data']=DB::table('customer_skills as cs')->select('cat.category_name','cat.category_id')->join('category as cat','cat.category_id','=','cs.category_id')->groupBy('cs.category_id')->where('cs.customer_id',$id)->get();
         $data['title']='User Detail';
        return view('admin.user-detail',$data);
    }
    
    //function to get users
    public function users(){
        $data['users']=DB::table('customers')->get();
        $data['title']='List Users';
        return view('admin.users-list',$data);
    }
}
