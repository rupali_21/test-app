<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helper\CommonFunction; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;   
use App\Admin;  
use Mail;
use Password; 
use DB; 

class CategoryController extends Controller
{
     private $entryDate;
    public function __construct()
    { 
         $this->entryDate = date("Y-m-d H:i:s");
    }

    //function to get all main category
    public function category(){
        $data['title']='Category'; 
        $data['category']=DB::table('category')->where('parent_id',0)->get();
        return view('admin.category',$data); 
    }

    //function to get all subcategory
    public function subcategory(){
        $data['title']='Subcategory';  
        $data['subcategory']=DB::table('category')->where('parent_id','!=',0)->get();
        return view('admin.subcategory',$data); 
    }
    
    
    
}
