<?php

namespace App\Http\Controllers\User;
 

use App\Http\Controllers\Controller;
use validator;
use DB;
use App\Helper\CommonFunction; 
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    private $entryDate;
    public function __construct()
    {
        //$this->middleware('guest');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
         $this->entryDate = date("Y-m-d H:i:s");
    }

    public function dashboard(){
        $data['title']='Welcome';
        return view('user.index',$data);
    }

    //function to register new user
    public function register(Request $request){
        $data['title']='Register';
        if(isset($_POST['submit'])){ 
            extract($_POST);
            $rules = [
                'first_name' => 'required|regex:/^[a-zA-Z]+$/|min:3|max:30',
                'last_name' => 'required|regex:/^[a-zA-Z]+$/|min:3|max:30', 
                'phone' => 'required|unique:customers|regex:/^[0-9]+$/|min:7|max:15',
                'email' => 'required|unique:customers|min:3|max:100|email',
                'zip'=> 'required|min:5|max:7',
                'street'=>'required|min:3|max:100',
                'city_id'=>'required',
                'state_id'=>'required', 
                // 'category_id'=>'required'
            ];
            $customMessages = [
                'first_name.required'=>'First name must not be empty',
                'first_name.regex'=>'First name must be alphabet only',
                'first_name.min'=>'First name must be at least 3 characters',
                'first_name.max'=>'First name must be less than 30 characters',
                'last_name.required'=>'Last name must not be empty',
                'last_name.regex'=>'Last name must be alphabet only',
                'last_name.min'=>'Last name must be at least 3 characters',
                'last_name.max'=>'Last name must be less than 30 characters', 
                'phone.required'=>'Phone no must not be empty',
                'phone.unique'=>'Phone no must be Unique', 
                'phone.regex'=>'Phone no must be digits only',
                'phone.min'=>'Phone no must be at least 7 digits ',
                'phone.max'=>'Phone no must be less than or equal to 15 digits', 
                'email.required'=>'Email must not be empty',
                'email.unique'=>'Email must be Unique',
                'email.email'=>'Email must be in valid format ',
                'email.min'=>'Email must be at least 3 characters',
                'email.max'=>'Email must be less than 100 characters', 
                'zip.required'=>'Zip code must not be empty',  
                'zip.min'=>'Zip code must be at least of 5 digit',
                'zip.max'=>'Zip code must be less than 7 digit',                
                'city_id.required'=>'City must not be empty',
                'state_id.required'=>'State must not be empty',
                'street_name.min'=>'Street name must be at least 3 characters',
                'street_name.max'=>'Street name must be less than 100 characters',
                'street_name.required'=>'Street name must not be empty',
                // 'category_id.required'=>'Category must not be empty .', 
            ];
            $this->validate($request, $rules, $customMessages);

            $status=false;

            //create user
             $data=array( 
                'first_name'=>$first_name,
                'last_name'=>$last_name,
                'street'=>$street,
                'city'=>$city_id,
                'state'=>$state_id,
                'zip_code'=>$zip,
                'phone'=>$phone,
                'email'=>$email,
                'status'=>1,
                'created_at'=>$this->entryDate,
                'updated_at'=>$this->entryDate,
            ); 
            $id=DB::table('customers')->insertGetId($data); 
            if($id){
                //create customer category skills
                if(count($parent_id)){
                    $i=0;
                    foreach($parent_id as $cat){
                        $skill_data=array(
                            'customer_id'=>$id,
                            'category_id'=>$cat,  
                            'skill_id'=>$skill_id[$i],
                            'skill_name'=>$category_name[$i], 
                            'skill_scores'=>$skill_score[$i], 
                            'created_at'=>$this->entryDate,
                            'updated_at'=>$this->entryDate,
                        ); 
                        $skill=DB::table('customer_skills')->insertGetId($skill_data); 
                        if($skill){
                            $status=true;
                        }
                        $i++;
                    }
                }
                if($status==true){
                    //send mail
                    $data=array(
                        'name' => $first_name.' '.$last_name,
                        'msg'=>'Welcome to TEST APP you are registered successfully',
                        'phone'=>$phone, 
                        'email' => $email
                    );
                
                    Mail::to($email)->send(new SendMail($data));

                    $request->session()->flash('success', 'User registered successfully.');
                     return redirect()->back();
                } else{
                    $request->session()->flash('error', 'Somethink went wrong.Please try again.');
                    return redirect()->back();
                }
            }else{
                    $request->session()->flash('error', 'Somethink went wrong.Please try again.');
                    return redirect()->back();
            } 
        }else{
            $data['category']=DB::table('category')->where('parent_id',0)->get();
            $data['state']=DB::table('states')->get();

            return view('user.register',$data);
        }
    }

    //get city on bases of state
     public function City(Request $request){
        $states = DB::table('cities')->where('state_id',$_POST['state_id'])->orderBy('city')->get();
        echo json_encode($states);
    }
    
    //get category skill on bases of category
    public function skills(Request $request){
        $data=array();
        $category=$request->selectedCategory;  
        foreach($category as $cat){ 
                $category_name=DB::table('category')->select('category_name')->where('category_id',$cat)->first();
                $category_skill=DB::table('category')->select('category_id as skill_id','parent_id','category_name')->where('parent_id',$cat)->get();
                // $category_skill['category']=$category_name;
                $data[]=$category_skill;
        }
        $response=array('data'=>$data,'cat_name'=>$category_name);
        echo json_encode($response); 
    }
}
