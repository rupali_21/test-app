<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth; 
use Validator; 
use App\Http\Middleware\EncryptCookies;
use Cookie;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

     public function login(Request $request)
    {
        // 
        $data['title']= 'Login';
        if(isset($_POST['submit'])){
            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                $request->session()->flash('error','Validation error occurred');
                return redirect()->back()->withInput();  
            } 

            extract($_POST);
            if (Auth::attempt(['email' => request('username'), 'password' => request('password'),'user_type'=>'Admin'])) {
                $login = true;
            } else if (Auth::attempt(['mobile_number' => request('username'),'password' => request('password'),'user_type'=>'Admin'])) {
                $login = true;
            }else if (Auth::attempt(['user_name' => request('username'),'password' => request('password'),'user_type'=>'Admin'])) {
                $login = true;
            }

            if (isset($login)) {
                $user = Auth::user(); 
                if ($user->status == 0) {
                    $request->session()->flash('error', 'Your account is deactivated');
                    return redirect()->back()->withInput(); 
                } 
                else { 
                    
                    $userData = array(
                        'user_id' => $user->id,  
                        'user_name'=>$user->user_name,
                        'email'=>$user->email,
                        'mobie_number'=>$user->mobile_number,
                        'profile_image'=>$user->profile_image, 
                    );
                    
                    //Converting NULL to "" String
                    array_walk_recursive($userData, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    }); 

                    $request->session()->flash('success', 'Login successful');
                    return redirect('admin/dashboard'); 
                }
            } else {
                $request->session()->flash('error', 'Invalid login credentials');
                return redirect()->back()->withInput();  
            } 
        }else{
            return view('admin.auth.login',$data);
        }
    }

     
}
