<?php  

use DB;

class CommonFunction
{  
      
    // Used to Encrypt ID
    public static function encryptId($id){
        return substr(md5($id), 0, 6).dechex($id);
    }

    // Used to Dycrypt ID 
    public static function decryptId($id){
        $md5_8 = substr($id, 0, 6);
        $real_id = hexdec(substr($id, 6));
        return ($md5_8==substr(md5($real_id), 0, 6)) ? $real_id : 0;
    } 

    //Check data is registered or not with user type
    public static function checkValueExist($table,$value = null, $field = null, $user_type = null)
    {
        $row = DB::table($table)
            ->select('*')
            ->where([$field => $value, 'user_type' => $user_type])
            ->first();
        if (!empty($row)) {
            return $row;
        } else {
            return '';
        }
    }

    //This funciton is used to pass column name in views blade files
    public static function GetSingleField($table, $select, $field, $value)
    {
        $result = DB::table($table)->where([$field => $value])->select($select)->first();
        if (!empty($result)) {
            return $result->$select;
        } else {
            return '';
        }
    }

    //This funciton is used to get single row
    public static function GetSingleRow($table, $field, $value)
    {
        return DB::table($table)->where([$field => $value])->first();
    }


    //Check Phone No/Email/userName is registered or not with user type
    public static function checkMobileExist($mobile_number)
    {
        $row = DB::table('users')
            ->select('id')
            ->where(['mobile_number' => $mobile_number])
            ->first();
        if (!empty($row)) {
            return $row;
        } else {
            return '';
        }
    }

    //Check Phone No/Email/userName is registered or not with user type
    public static function checkEmailExist($email)
    {
        $row = DB::table('users')
            ->select('id')
            ->where(['email' => $email])
            ->first();
        if (!empty($row)) {
            return $row;
        } else {
            return '';
        }
    }
 
   
}
