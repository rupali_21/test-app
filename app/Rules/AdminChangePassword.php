<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth;

class AdminChangePassword implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!(Hash::check($value, Auth::user()->password))) {

            return false;
        }
        else{
            return true;
        }
//        $if_exist = DB::table('lg_users')->where(array('password'=>Hash::make($value),'user_type'=>'A','id'=>Auth::user()->id))->first();
//        dd($if_exist);
//        if($if_exist){
//            return true;
//        }
//        else{
//            return false;
//        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is wrong.';
    }
}
